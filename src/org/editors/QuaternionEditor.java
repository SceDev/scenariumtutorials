package org.editors;


import org.beanmanager.editors.PropertyEditor;
import org.records.Quaternion;

import javafx.beans.InvalidationListener;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;

public class QuaternionEditor extends PropertyEditor<Quaternion> {

	private TextField textFieldA;
	private TextField textFieldB;
	private TextField textFieldC;
	private TextField textFieldD;

	@Override
	protected Region getCustomEditor() {
		if(getValue() == null)
			setValue(new Quaternion(0, 0, 0, 0));
		Quaternion quat = getValue();
		textFieldA = new TextField(Double.toString(quat.a));
		textFieldB = new TextField(Double.toString(quat.b));
		textFieldC = new TextField(Double.toString(quat.c));
		textFieldD = new TextField(Double.toString(quat.d));
		textFieldA.setPrefWidth(80);
		textFieldB.setPrefWidth(80);
		textFieldC.setPrefWidth(80);
		textFieldD.setPrefWidth(80);
		InvalidationListener il = e -> {
			try {
				double a = Double.parseDouble(textFieldA.getText());
				double b = Double.parseDouble(textFieldB.getText());
				double c = Double.parseDouble(textFieldC.getText());
				double d = Double.parseDouble(textFieldD.getText());
				Quaternion val = getValue();
				if(val.a != a || val.b != b || val.c != c || val.d != d)
					setValue(new Quaternion(a, b, c, d));
			}catch(NumberFormatException ex) {
				System.err.println("Wrong formatting");
			}
		};
		textFieldA.textProperty().addListener(il);
		textFieldB.textProperty().addListener(il);
		textFieldC.textProperty().addListener(il);
		textFieldD.textProperty().addListener(il);
		HBox hb = new HBox(textFieldA, textFieldB, textFieldC, textFieldD);
		return hb;
	}
	
	@Override
	public boolean hasCustomEditor() {
		return true;
	}
	
	@Override
	public void updateCustomEditor() {
		Quaternion value = getValue();
		textFieldA.setText(Double.toString(value.a));
		textFieldB.setText(Double.toString(value.b));
		textFieldC.setText(Double.toString(value.c));
		textFieldD.setText(Double.toString(value.d));
	}

	@Override
	public String getAsText() {
		Quaternion quat = getValue();
		return quat == null ? "" : quat.a + " " + quat.b + " " + quat.c + " " + quat.d + " ";
	}

	@Override
	public void setAsText(String text) {
		if (text == null || text.isEmpty() || text.equals(String.valueOf((Object) null)))
			setValue(null);
		else {
			String[] splits = text.split(" ");
			setValue(new Quaternion(Double.parseDouble(splits[0]), Double.parseDouble(splits[1]), Double.parseDouble(splits[2]), Double.parseDouble(splits[3])));
		}
	}

}
