/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.datastream;

import java.io.DataInput;
import java.io.IOException;

import org.records.Quaternion;
import org.scenarium.filemanager.datastream.input.DataFlowInputStream;

public class QuaternionDataInputStream extends DataFlowInputStream<Quaternion> {
	
	public QuaternionDataInputStream() {}

	@Override
	public Quaternion pop() throws IOException {
		DataInput di = this.dataInput;
		return new Quaternion(di.readDouble(), di.readDouble(), di.readDouble(), di.readDouble());
	}

}
