/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.datastream;

import java.io.IOException;

import org.records.Quaternion;
import org.scenarium.filemanager.datastream.output.DataFlowOutputStream;

public class QuaternionDataOutputStream extends DataFlowOutputStream<Quaternion> {
	
	public QuaternionDataOutputStream() {}

	@Override
	public void push(Quaternion q) throws IOException {
		dataOutput.writeDouble(q.a);
		dataOutput.writeDouble(q.b);
		dataOutput.writeDouble(q.c);
		dataOutput.writeDouble(q.d);
	}
}
