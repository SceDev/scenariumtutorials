package org.toolBars;

import org.editors.QuaternionEditor;
import org.records.Quaternion;
import org.scenarium.display.toolbarclass.ExternalTool;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;

public class QuaternionCalculator extends ExternalTool{

	@Override
	public Region getRegion() {
		QuaternionEditor qe1 = new QuaternionEditor();
		QuaternionEditor qe2 = new QuaternionEditor();
		QuaternionEditor qeRes = new QuaternionEditor();

		HBox qe1Box = new HBox(new Label("Quaternion 1: "), qe1.getEditor());
		HBox qe2Box = new HBox(new Label("Quaternion 2: "), qe2.getEditor());
		HBox qeResBox = new HBox(new Label("Résultat: "), qeRes.getEditor());
		
		Button computeButton = new Button("Calculer");
		computeButton.setOnAction(e -> {
			Quaternion q1 = qe1.getValue();
			Quaternion q2 = qe2.getValue();
			Quaternion qRes = new Quaternion(q1.a + q2.a, q1.b + q2.b, q1.c + q2.c, q1.d + q2.d);
			qeRes.setValue(qRes);
		});
		return new VBox(qe1Box, qe2Box, computeButton, qeResBox);
	}

}
