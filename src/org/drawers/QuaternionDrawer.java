package org.drawers;

import org.beanmanager.editors.PropertyInfo;
import org.records.Quaternion;
import org.scenarium.display.ScenariumContainer;
import org.scenarium.display.drawer.TheaterPanel;
import org.scenarium.timescheduler.Scheduler;

import javafx.geometry.Dimension2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class QuaternionDrawer extends TheaterPanel{
	@PropertyInfo(info = "Color of the line")
	private Color lineColor = Color.RED;

	private Line line = new Line();

	@Override
	public void initialize(ScenariumContainer container, Scheduler scheduler, Object drawableElement, boolean autoFitIfResize) {
		super.initialize(container, scheduler, drawableElement, autoFitIfResize);
		line.setStrokeWidth(3);
		updateLineColor();
		getChildren().add(line);
	}
	
	@Override
	public void fitDocument() {
		
	}

	@Override
	public Dimension2D getDimension() {
		return new Dimension2D(640, 480);
	}

	@Override
	public String[] getStatusBarInfo() {
		return null;
	}

	@Override
	protected void paint(Object dataElement) {
		Quaternion quat = (Quaternion) dataElement;
		line.setStartX(getWidth() / 2 + quat.a);
		line.setStartY(getHeight() / 2 + quat.b);
		line.setEndX(getWidth() / 2 + quat.c);
		line.setEndY(getHeight() / 2 + quat.d);
	}

	@Override
	protected boolean updateFilterWithPath(String[] filterPath, boolean value) {
		return true;
	}
	
	@Override
	public void dispose() {
		super.dispose();
		getChildren().remove(line);
		line = new Line();
	}

	public Color getLineColor() {
		return lineColor;
	}

	public void setLineColor(Color lineColor) {
		this.lineColor = lineColor;
		updateLineColor();
	}

	private void updateLineColor() {
		line.setStroke(lineColor);
		line.setFill(lineColor);
	}
}
