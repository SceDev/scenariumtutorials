package org;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.datastream.QuaternionDataInputStream;
import org.datastream.QuaternionDataOutputStream;
import org.drawers.QuaternionDrawer;
import org.editors.QuaternionEditor;
import org.operator.tutorials.tutoio.DynamicIO;
import org.operator.tutorials.tutoio.FirstOperator;
import org.operator.tutorials.tutoio.StaticIO;
import org.operator.tutorials.tutoio.TimeStamping;
import org.operator.tutorials.tutoio.VariadicOutput;
import org.operator.tutorials.tutoproperties.EvolvedPropertiesOperator;
import org.operator.tutorials.tutoproperties.OperatorWithProperties;
import org.records.Quaternion;
import org.scenarium.ClonerConsumer;
import org.scenarium.DataStreamConsumer;
import org.scenarium.EditorConsumer;
import org.scenarium.PluginsSupplier;
import org.scenarium.Scenarium;
import org.scenarium.display.ToolBarInfo;
import org.scenarium.display.drawer.TheaterPanel;
import org.toolBars.QuaternionCalculator;

import javafx.scene.input.KeyCode;

public class ScenariumTutorials implements PluginsSupplier{
	
	public static void main(String[] args) {
		Scenarium.main(args);
	}
	
	@Override
	public void populateOperators(Consumer<Class<?>> operatorConsumer) {
		// tutoio
		operatorConsumer.accept(FirstOperator.class);
		operatorConsumer.accept(StaticIO.class);
		operatorConsumer.accept(DynamicIO.class);
		operatorConsumer.accept(VariadicOutput.class);
		operatorConsumer.accept(TimeStamping.class);

		// tutoproperties
		operatorConsumer.accept(OperatorWithProperties.class);
		operatorConsumer.accept(EvolvedPropertiesOperator.class);
	}
	
	@Override
	public void populateCloners(ClonerConsumer clonerConsumer) {
		clonerConsumer.accept(Quaternion.class, o -> o);
	}
	
	@Override
	public void populateEditors(EditorConsumer editorConsumer) {
		editorConsumer.accept(Quaternion.class, QuaternionEditor.class);
	}
	
	@Override
	public void populateDrawers(BiConsumer<Class<? extends Object>, Class<? extends TheaterPanel>> drawersConsumer) {
		drawersConsumer.accept(Quaternion.class, QuaternionDrawer.class);
	}
	
	public void populateToolBars(Consumer<ToolBarInfo> toolBarConsumer) {
		toolBarConsumer.accept(new ToolBarInfo(QuaternionCalculator.class, KeyCode.Y, true, null));
	};
	
	// Not yet implemented
//	@Override
//	public void populateScenarios(BiConsumer<Class<? extends Scenario>, String[]> scenarioConsumer) {
//		scenarioConsumer.accept(QuaternionScenario.class, new String[] {"quaternion"});
//	}
	
	@Override
	public void populateDataStream(DataStreamConsumer dataStreamConsumer) {
		dataStreamConsumer.accept(Quaternion.class, QuaternionDataInputStream.class, QuaternionDataOutputStream.class);
	}
	
	// Not yet implemented
//	@Override
//	public void populateFileRecorder(BiConsumer<Class<?>, Class<? extends FileStreamRecorder>> fileRecorderConsumer) {
//		fileRecorderConsumer.accept(t, u);
//	}
}
