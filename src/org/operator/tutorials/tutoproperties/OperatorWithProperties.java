/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.operator.tutorials.tutoproperties;

import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.NumberInfo;

public class OperatorWithProperties {
	private int a;				//	You can add properties by declaring the variable private and adding the accessors
	
	@PropertyInfo(index = 0, info = "A sub bean property")		// You can add annotations to give information for the editor
	@NumberInfo(min = 0, max = 10, controlType = ControlType.SPINNER_AND_SLIDER)		
	private double b = 4.0;		//	You can specify default value

	public void birth() {
		System.out.println("Je birth");
	}

	public Integer process(Integer value) {
		return (int) (value + a + b + 8);
	}

	public void death() {
		System.out.println("Je death");
	}

	// Accessors for the property A
	public int getA() {
		return a;
	}
	
	public void setA(int a) {
		this.a = a;
	}

	// Accessors for the property B
	public double getB() {
		return b;
	}

	public void setB(double b) {
		this.b = b;
	}
}
