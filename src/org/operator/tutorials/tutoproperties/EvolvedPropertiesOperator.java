/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.operator.tutorials.tutoproperties;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class EvolvedPropertiesOperator extends EvolvedOperator {
	private int[] tab;											// You can create properties containing base types
	private SubBean subBean; 									// You can create Java beans properties
	public void birth() {}

	public Integer process(Integer value) {
		if(this.tab != null && this.tab.length > 0) {
			return (int) (value + this.tab[0]);
		}
		return null;
	}
	
	public void death() {}

	public int[] getTab() {
		return this.tab;
	}

	public void setTab(int[] tab) {
		var oldValue = this.tab;
		this.tab = tab;
		// You can respect the principle of PropertyChangeSupport to be able to define these properties as outputs
		// or to see the evolution of this variable even if it is changed without passing through Scenarium
		pcs.firePropertyChange("tab", oldValue, tab);
	}

	public SubBean getSubBean() {
		return subBean;
	}

	public void setSubBean(SubBean subBean) {
		this.subBean = subBean;
	}
}
