/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.operator.tutorials.tutoio;

public class StaticIO {
	private Double y;

	public void birth() {}

	public Integer process(Integer x, Double y) { 	// You can add other inputs
		this.y = y;									// You can store an entry in a field and then manipulate it for static outputs
		if (x != null) {							// Please note that only the available inputs give data
			return x + 8;
		}
		return null;								// Returning null implies that the block does not return data on the corresponding output
	}
	
	public Double getOutputB() {						// Declaring a method getOutputXXX means creating a output with the name XXX
		return y;
	}

	public void death() {}
}
