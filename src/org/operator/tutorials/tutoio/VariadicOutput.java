/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.operator.tutorials.tutoio;

public class VariadicOutput {

	public void birth() {}

	public Integer process(Integer si, Integer... vi) {		// Just declare the method as a variadic method
		if(vi[0] != null)
			return vi[0] + 5;
		return null;
	}

	public void death() {}
}
