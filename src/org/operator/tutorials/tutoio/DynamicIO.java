/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.operator.tutorials.tutoio;

import java.util.Arrays;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;

public class DynamicIO extends EvolvedOperator {		// You have to inherit 'EvolvedOperator' to access the dynamic and static outputs.
	
	@Override
	public void updateIOStructure() throws Exception {			// The initStruct method is called at the initialization of a block. 
														// You can override this method to customize inputs and outputs.
		updateInputs(new String[] {"di-0", "di-1"}, new Class<?>[] {Integer.class, Double.class});
		updateOutputs(new String[] {"do-0"}, new Class<?>[] {Integer.class});
	}

	public void birth() {}
	
	@ParamInfo(in = "si")								// Set static input name in order to be able to trigger it after
	public Integer process(Integer value) {
		// Additional inputs can be obtained using 'getAdditionalInputs' methods. The array size is equal to the number of input.
		Object[] additionalInputs = getAdditionalInputs();
		System.out.println("AdditionalInputs: " + Arrays.toString(additionalInputs));
		
		// You can trigger the outputs in two different ways:
		// 1- triggerOutput
		triggerOutput(new Object[] {Integer.valueOf(2), Integer.valueOf(5)}, new long[] {getMaxTimeStamp(), getMaxTimeStamp()});
		// 2- openOutputBuffer -> setOutputData nbTriggerableOutput times, flushOutputBuffer
		openOutputBuffer(true);			// Avoid creating of new array each time
		setOutputData("si", Integer.valueOf(2), getMaxTimeStamp());
		setOutputData("do-0", Integer.valueOf(5), getMaxTimeStamp());
		flushOutputBuffer();
		// Two data were triggered at this level
		return null;		// Return null if you don't want to trigger a third output
	}

	public void death() {}

}
