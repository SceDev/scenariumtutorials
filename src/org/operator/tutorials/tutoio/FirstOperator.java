/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.operator.tutorials.tutoio;

public class FirstOperator {

	public void birth() {
		System.out.println("Je birth");
	}

	public Integer process(Integer value) {
		return value + 8;
	}

	public void death() {
		System.out.println("Je death");
	}

}
