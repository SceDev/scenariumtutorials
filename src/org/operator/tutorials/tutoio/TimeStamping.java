/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.operator.tutorials.tutoio;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class TimeStamping extends EvolvedOperator { 	// You can inherit 'EvolvedOperator' to get access to additional methods

	public void birth() {}

	public long process(Integer value) {
		return getTimeStamp(0); 			// Show the timestamp associated with the data 'value'
	}

	public void death() {}

}
