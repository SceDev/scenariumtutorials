package org.records;

public class Quaternion {
	public final double a;
	public final double b;
	public final double c;
	public final double d;
	
	public Quaternion(double a, double b, double c, double d) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}

	@Override
	public String toString() {
		return "Quaternion [a=" + a + ", b=" + b + ", c=" + c + ", d=" + d + "]";
	}
}
