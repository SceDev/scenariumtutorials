import org.ScenariumTutorials;
import org.scenarium.PluginsSupplier;

module ScenariumTutorials {
	uses PluginsSupplier;
	provides PluginsSupplier with ScenariumTutorials;
	
	exports org.editors;
	exports org.operator.tutorials.tutoio;
	exports org.operator.tutorials.tutoproperties;
	exports org.drawers;
	exports org.records;
	exports org.toolBars;
	exports org.datastream;
	
	requires javafx.base;
	requires javafx.controls;
	requires javafx.graphics;
	requires transitive Scenarium;
}